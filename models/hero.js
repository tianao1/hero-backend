'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Hero extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    toJSON(){
      return{...this.get(), createdAt: undefined, updatedAt: undefined}
    }
  }
  Hero.init({
    name: {
      type: DataTypes.STRING,
      allowNull:false,
    } 
  }, {
    sequelize,
    tableName:'heroes',
    modelName: 'Hero',
  });
  return Hero;
};