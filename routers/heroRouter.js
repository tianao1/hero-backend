const express = require('express');
const HeroController = require('../controllers/heroController')
const heroRouter = express.Router();

heroRouter.get('/', HeroController.getHeroes)

heroRouter.get('/:id', HeroController.getSingleHero)

heroRouter.post('/', HeroController.createHero)
heroRouter.put('/', HeroController.updateHero)

heroRouter.delete('/:id', HeroController.deleteHero)

module.exports = heroRouter;