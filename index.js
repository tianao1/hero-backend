const express = require('express');
const cors = require('cors')
const {sequelize} = require('./models');
const heroRouter = require('./routers/heroRouter')
const app  = express();
app.use(express.json());
app.use(cors());
app.use('/heroes', heroRouter);

const port = 3000;


app.listen(port, async () => {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
    console.log(`Example app listening on port ${port}`);
})