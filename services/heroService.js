const {Sequelize,sequelize,Hero} = require('../models');
const Op = Sequelize.Op;

class HeroService {
    static async getHeroes(term){

        return await Hero.findAll({
            where: {
                name: {[Op.like]: `%${term}%`}
            }
        });

    }

    static async getSingleHero(id){

        return await Hero.findOne({where:{id}});
    }

    static async createHero(name){

        return await Hero.create({name});
    }

    static async updateHero(name, id){
        return await Hero.update({name}, {where:{id}});
    }

    static async deleteHero(id){
        const hero = await Hero.findOne({where: {id}});
        await hero.destroy();
        return;
    }
}

module.exports = HeroService;