const HeroService = require('../services/heroService')
class HeroController {

    errHandler(err, message){

        console.log(err);
        return res.status(500).json({error: message});
    }

    static async getHeroes(req, res){
        let term = req.query.name;
        if(!term){
            term = '';
        }

        try {
            const heroes = await HeroService.getHeroes(term);
            return res.json(heroes);
        } catch(err) {
            errHandler(err, 'Cannot get heroes');
        }
    }

    static async getSingleHero(req, res){
        const id = req.params.id;

        try {
            const hero = await HeroService.getSingleHero(id);
            if(!hero){
                return res.status(500).json({error: `There is no such hero with id=${id}`});
            }
            return res.json(hero);
        } catch(err){
            errHandler(err, `Cannot get hero with id=${id}`);
        }
    }

    static async createHero(req, res){
        const {name} = req.body;

        try {
            const hero = await HeroService.createHero(name);
            return res.json(hero);
        } catch(err){
            errHandler(err, 'Cannot create hero')
        }
    }

    static async updateHero(req, res){
        const {name, id} = req.body;
        try {
            await HeroService.updateHero(name, id);
            return res.json({success: true});
        } catch(err){
            errHandler(err, `Cannot update hero with id=${id}`)
        }
    }

    static async deleteHero(req, res){
        const id = req.params.id;
        try {
            await HeroService.deleteHero(id);
            return res.json({success: true});
        } catch(err){
            errHandler(err, `Cannot delete hero with id=${id}`)
        }
    }
}

module.exports = HeroController;